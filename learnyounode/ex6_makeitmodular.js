var dirLister = require('./dirlister/ls_by_ext.js');
var dir = process.argv[2];
var filterExt = process.argv[3];

dirLister(dir, filterExt, function (err, data){
    if (err) {
        console.log("Ooops! An error occured... ", err);
    }    
    for (var file in data) {
        console.log(data[file]);
    }
});

/*
#DEFAULT SOLUTION:

var filterFn = require('./solution_filter.js')
var dir = process.argv[2]
var filterStr = process.argv[3]

filterFn(dir, filterStr, function (err, list) {
  if (err)
    return console.error('There was an error:', err)

  list.forEach(function (file) {
    console.log(file)
  })
})
*/