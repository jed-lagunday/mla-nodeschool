var fs = require('fs');
var path = require('path');

var dir = process.argv[2];
var ext = '.' + process.argv[3];

fs.readdir(dir, function listFiles(err, files){
    if (err) {
        console.log('Ooops! An error occured...');
    } else {
        // for (var i = 0; i < files.length; i++) {
        //     if (path.extname(files[i]) == ext) {
        //         console.log(files[i]);
        //     }
        // }
        for (var file in files) {
            if (path.extname(files[file]) == ext) {
                console.log(files[file]);
            }
        }
    }
});