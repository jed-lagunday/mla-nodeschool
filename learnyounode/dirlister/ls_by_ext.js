var fs = require('fs');
var path = require('path');

module.exports = function (loc, ext, callback) {
    ext = '.' + ext;
    fs.readdir(loc, function listFiles(err, files){
        if (err) {
            return callback(err);
        } else {
            var data = [];
            for (var file in files) {
                if (path.extname(files[file]) == ext){
                    data.push(files[file]);
                }
            }
            return callback(null, data);
        }
    });
};

/*
#DEFAULT SOLUTION

var fs = require('fs')
var path = require('path')

module.exports = function (dir, filterStr, callback) {
    fs.readdir(dir, function (err, list) {
        if (err)
        return callback(err)

        list = list.filter(function (file) {
        return path.extname(file) === '.' + filterStr
        })

        callback(null, list)
    })
}
*/
